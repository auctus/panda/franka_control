# Continous integration

[![pipeline status](https://gitlab.inria.fr/auctus/panda/franka_control/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/franka_control/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:franka-control)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:franka-control&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:franka-control)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Afranka_control
- Documentation : https://auctus.gitlabpages.inria.fr/panda/franka_control/index.html


# Franka control

Modified version of the franka_control from https://github.com/frankaemika/franka_ros/tree/kinetic-devel/franka_control

Added :

```
    // Set default collision behavior
    robot.setCollisionBehavior(
      {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}}, {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}},
      {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}}, {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}},
      {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}}, {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}},
      {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}}, {{1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0}});
```
 To remove the soft collision behaviour set by Franka

```
    ROS_INFO_STREAM("Performing error receovery");
    try {
        robot.automaticErrorRecovery();
        ROS_WARN("Recovered from error");
    } catch (const franka::Exception& ex) {
        ROS_ERROR_STREAM(
          "JointVelocityExampleController: Exception getting state handle: " << ex.what());
        }
```

To perform an automatic error recovery each time a new franka_control is instantiated.